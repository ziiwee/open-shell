# open-shell

2ac43d1e28e2
b4ce2ad1b2c7

https://content.hongmengshijie.net/windows/FastOrange.exe

安装 nodejs （CentOS）
```
yum -y install nodejs npm --enablerepo=epel
```

安装 mysql （Docker）
```
docker run --name mysql -e MYSQL_ROOT_PASSWORD=Root@1234! -itdp 3306:3306 -v mysql:/etc/mysql/conf.d mysql:5.6
```

安装 JDK8 
```
# Debian, Ubuntu
apt-get install openjdk-8-jre
apt-get install openjdk-8-jdk

# Fedora, Oracle Linux, Red Hat Enterprise Linux
# JRE
yum install -y java-1.8.0-openjdk
# JDK
yum install -y java-1.8.0-openjdk-devel
```
